*** Settings ***
Library     SeleniumLibrary

*** Variable ***
${BROWSER}              chrome
${URL}                  https://robotizandotestes.blogspot.com.br/
${CABEÇALHO}            id=Header1
${BOTAO_LUPA}           css=.search-expand.touch-icon-button
${CAMPO_PESQUISAR}      css=.search-input>input
${BOTAO_PESQUISAR}      css=.search-action.flat-button
${TITULO}               xpath=.//*[@id='Blog1']/div/article/div[1]/div/h3

*** Keywords ***
#### DADO
Que esteja na tela HOME do blog robotizando testes
    Open Browser    ${URL}  ${BROWSER}
    Wait Until Element Is Visible   ${CABEÇALHO}
    Title Should Be     Robotizando Testes

Que esteja na tela de resultado da pesquisa pela postagem "${TITULO_POSTAGEM}"
    Page Should Contain     ${TITULO_POSTAGEM}

#### QUANDO
Pesquisar pela palavra "${BUSCA}"
    Click Element   ${BOTAO_LUPA}
    Input Text      ${CAMPO_PESQUISAR}    ${BUSCA}
    Click Element   ${BOTAO_PESQUISAR}
    Wait Until Page Contains    Mostrando postagens que correspondem à pesquisa por ${BUSCA}

Clicar no link da postagem "${POSTAGEM}"
    Click Element    xpath=//*[@id="Blog1"]//a[text()='${POSTAGEM}']

#### ENTÃO
A postagem "${TITULO_POSTAGEM}" deve ser listada no resultado da pesquisa
    Page Should Contain     ${TITULO_POSTAGEM}
    Capture Page Screenshot

A tela da postagem "${TITULO_POSTAGEM}" deve ser mostrada
    Wait Until Element Is Visible  ${TITULO}
    Title Should Be  ${TITULO_POSTAGEM}
    Capture Page Screenshot

#### TEARDOWN
Fechar Navegador
    Close Browser
